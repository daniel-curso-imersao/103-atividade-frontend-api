const logradouro = document.querySelector("#logradouro");
const complemento = document.querySelector("#complemento");
const bairro = document.querySelector("#bairro");
const cidade = document.querySelector("#cidade");
const estado = document.querySelector("#estado");
const btnConsultaCEP = document.querySelector("#buscarCEP");
const CEP = document.querySelector("#CEP");

function extrairJSON(resposta) {
    return resposta.json();
}

function preencherDados(dados) {
    logradouro.innerHTML = dados.logradouro;
    complemento.innerHTML = dados.complemento;
    bairro.innerHTML = dados.bairro;
    cidade.innerHTML = dados.localidade;
    estado.innerHTML = dados.uf;
}

function consultarCEP() {
    let cep = document.querySelector("#CEP").value;
    fetch(`https://viacep.com.br/ws/${cep}/json/`).then(extrairJSON)
        .then(preencherDados);
}

CEP.value = "";
btnConsultaCEP.onclick = consultarCEP;